# SuperMario

## Instructions
1. Download and install **Java SE Runtime Environment 8** from [here](https://www.oracle.com/technetwork/java/javase/downloads/jre8-downloads-2133155.html)
2. Download **SuperMario.jar** from this repository's downloads section
3. Now you can execute **SuperMario.jar** by running the following command: `$ java -jar SuperMario.jar`

## Libraries used
* [JUnit 4](https://github.com/junit-team/junit4)
* [libGDX](https://github.com/libgdx/libgdx)

## Programs used
* [LibGDX Texture Packer](https://github.com/crashinvaders/gdx-texture-packer-gui)
* [Tiled](https://www.mapeditor.org/)

## Authors
* [Fontanella Simone](https://bitbucket.org/SimoneFontanella/)
* [Pesaresi Luca](https://bitbucket.org/liuk26712/)
* [Avagnano Marco](https://bitbucket.org/Marcoavagnano/)
* [Zani Thomas](https://bitbucket.org/ThomasZani/)
